<script>MathJax = {tex: {inlineMath: [['$', '$']]}};</script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
# Averaging tool of the HFLAV b2charm and rare decays groups

The tool reads **parameters** and **publications** with **measurements** that are sensitive to the parameters from **json files**.
It constructs a likelihood function and minimizes it to find the best estimates of the parameter **averages**.
The result can be output in form of **latex** or **html** files.

## Methodology

A negative log likelihood function is constructed from the considered measurements.
In case of a measurement with central value _x_ and symmetric uncertainty &sigma; for the parameter _p_ the negative log likelihood function is _NLL = (p - x)<sup>2</sup> / 2&sigma;<sup>2</sup>_.
In case of a measurement with asymmetric uncertainties, _x<sup>+&sigma;<sub>+</sub></sup><sub>-&sigma;<sub>-</sub></sub>_, the negative log likelihood function is assumed to be _NLL = (p - x)<sup>2</sup> / 2V_ is used where _V = &sigma;<sub>-</sub><sup>2</sup>_ for _p-x < -&sigma;<sub>-</sub>_, _V = &sigma;<sub>+</sub><sup>2</sup>_ for _p-x > &sigma;<sub>+</sub>_, and _V_ is interpolated linearly between those regions.

Statistical and uncorrelated systematic uncertainties are added in quadrature.
Correlated systematic uncertainties are modeled by nuisance parameters.
In case a measurement with central value _x_ and correlated systematic uncertainty _&sigma;<sub>corr</sub>_ the offset _&sigma;<sub>corr</sub>n_ is added to _x_ where _n_ is a nuisance parameter with normal likelihood function.

Sometimes publications quote a central value estimate _x_ of parameter _p_ that is calculated from a measurement of _p/q_ (with normalization mode) or _pq_ (with daughter branching fraction).
If the value, _y_, of _q_ used for the calculation is known, the quoted result can be updated if a more recent central value estimate, _y'_ of _q_ is available.
In this case _py'/y_ or _py/y'_ is used in the negative log likelihood function instead of _p_ for a measurement with normalization mode or daughter branching fraction, respectively.
Here the width of the (asymmetric) Gaussian does not include the systematic uncertainty from _y_.
The uncertainty on _y'_ is taken into account by including it in the fit as an (external) parameter with corresponding likelihood function.

If a publication quotes a measurement of _p/q_ or _pq_ and a derived value of _p_, only one of them must be included in the averaging.
Generally the former is preferred because it has less dependence on external input and the framework can determine the derived value.

The minimization of the likelihood is done with [iminuit](https://github.com/scikit-hep/iminuit).
Minos is used to determine positive and negative uncertainties.
The uncertainties are symmetrized if both differ from the Hesse uncertainty by less than a given threshold which is 5% by default.

Two times the negative log likelihood value at the fit minimum is used to determine a total _&chi;<sup>2</sup>_ value and the contributions to it from measurements and nuisance parameters.
If the fit p-value is below 1% it is shown in the latex table.

## Installation

The averaging tool requires `python3`.

It is recommended to install the averaging tool in a virtual environment:

    python3 -m venv averaging_venv
    . averaging_venv/bin/activate

Then run the following commands in the `averaging` directory:

    pip install .


## Update

To get and install updates of the averaging that are available on the main branch in the git repository run the following commands in the `averaging` directory (with virtual environment activated):

    git pull
    pip install .


## Execution

Activate the python environment where you installed the averaging tool.
If you created the virtual environment in the directory `averaging_venv` use the following command:

    . averaging_venv/bin/activate

Then go to the data directory which contains the json files with parameters, publications, and measurements and execute the `averaging` module:

    python3 -m averaging

To see all command line options run

    python3 -m averaging --help

Important options are `--web` and `--latex` to produce a web page or latex output.
The latter requires the name of a json file that specifies the [structure](#structure) of the output as argument.

Preliminary measurements without journal publication are shown in blue in tables.
Measurements that are not (yet) included in the PDG average are shown in red.
For the average of parameter _p_ all direct measurements are included in the plot and their values are shown in tables.
This is also the case for measurements with a normalization mode, _p/q_, or with a daughter branching fraction, _pq_.
Here the value and uncertainty of the measurement of _p_ are determined with the fitted value of _q_.
In case a measurement depends on _p_ in some other way it is still included in the fit, but not in the plot and in tables the measured parameters instead of a derived value are shown.

The number of displayed digits of averages is chosen such that the uncertainty (or the larger of the uncertainties in case of asymmetric ones or the value in case of limits) has two significant digits.

### Web server

The command `hflav_webserver` triggers the web server to start. This must be preceded by a command like `python -m averaging --web` to make sense.

## Parameters

The parameters that will be fitted are read from the file `parameters.json` and any json file in or below the `parameters` directory.

Each parameter can have the following attributes

* `name` (string): A unique identifier of the parameter. Only alphanumeric (`A-Za-z0-9`) and the character `_.+-*()` are allowed. Note the [naming conventions](naming.md) used by HFLAV.
* `latex` (string): The latex code of the parameter. Note that backslashes must be escaped in json files. The `latex` attribute can be omitted if the `name` follows the [naming conventions](naming.md) and the latex code can be generated from it.
* `text` or `description` (string, optional): A text representation of the parameter.
* `pdg_id` or `pdgId` (string, optional): The [identifier used by PDG](https://pdg.lbl.gov/2020/pdgid/PDGIdentifiers-current.json).
* `comment` (string or list of strings, optional): A literal comment or an identifier of a comment that will be displayed as a footnote. Multiple comments can be given in a list.
* `revisions` (dictionary, optional): Central values and uncertainties of previous measurements (used as input in measurements entering averages) identified by a string.

Example json file:

    [
        {
            "name": "BR_B0_D-_pi+",
            "pdg_id": "S042:Desig=30"
        },
        {
            "name": "BR_B0_D**-_pi+",
            "comment": "$D^{**}$ refers to the sum of all the non-strange charm meson states with masses in the range 2.2 - 2.8 GeV$/c^2$",
            "pdg_id": "S042:Desig=357",
            "text": "B0 --> Dbar^**-() pi+",
            "latex": "{\\cal{B}} (B^{0} \\to D^{**-} \\pi^{+})",
            "revisions": {
                "PDG20": "1.9 +-0.9 e-3"
            }
        }
    ]

### External Parameters

The parameters that are used as external inputs for the averaging are read from the file `externals.json`.
External parameters with uncertainties are included as floating parameters in the fit.

In addition to the attributes of the normal parameters an external parameter must have one of the following attributes:

* `value` (string): Central value and (optional) uncertainties of the external parameter. The format in which value, uncertainties and units are given is described in the [expressions format section](#expression).
* `nll` (string): Formula of negative log likelihood function of the external parameter. The formula will be evaluated in python.

The `value` or `nll` can be omitted if the external parameter has a `pdg_id`.
In this case the value and uncertainty are taken from PDG.

### Nuisance Parameters

The auxiliary parameters that are included in the fit, but do not have a direct physical meaning are read from the file `nuisances.json`.
A normal distribution with mean 0 and width 1 is used as likelihood of nuisance parameters.

### Changing Parameter Names

The tool `hflav_rename` should be used to rename parameters and update json files consistently.
To see the command line options run

    hflav_rename --help

## Publications

The publications quoting measurements of the parameters of interest are read from any json file in or below the `publications` directory.

Each publication can have the following attributes

* `inspire` (integer): The identifier used by [InSPIRE HEP](https://inspirehep.net/).
* `arxiv` (string, optional): The identifier used by [arXiv](https://arxiv.org/).
* `journal` (dictionary, optional): Journal reference information:
    * `title` (string)
    * `volume` (string)
    * `id` (string)
    * `year` (integer)
* `doi` (string, optional): Digital object identifier.
* `altref` (array, required if neither `journal` nor `arxiv` is given): The first array element is a string with the reference name and the second array element a URL to the document.
* `experiment` (string): Name of the experiment or collaboration. Be careful not to use different spellings of the same name.
* `comment` (string or list of strings, optional): A literal comment or an identifier of a comment that will be displayed as a footnote. Multiple comments can be given in a list.
* `superseded` (bool, optional): If True all measurements in this publication are considered superseded.
* `bibtex` (string, optional): The bibtex of the publication. Note that it has to be a valid json string with proper escaping and in one line.
* `measurements` (array): The [measurements](#measurements) sensitive to the parameters of interest.

### Publication Data from Inspire

The command `hflav_publication` can be used to get many of the publication attributes from inspire.
It takes an inspire or arxiv identifier as argument and creates or updates a json file with file name given by the inspire ID with json extension.
For example the command

    hflav_publication 727062

produces the json file `727062.json` with the following content:

    {
        "inspire": 727062,
        "arxiv": "hep-ex/0609047",
        "journal": {
            "title": "Phys.Rev.Lett.",
            "volume": "98",
            "id": "131803",
            "year": 2007
        },
        "doi": "10.1103/PhysRevLett.98.131803"
        "experiment": "Belle",
        "bibtex": "@article{Chang:2006sd,\n    author = \"Chang, M. -C. and others\",\n    editor = \"Sissakian, Alexey and Kozlov, Gennady and Kolganova, Elena\",\n    collaboration = \"Belle\",\n    title = \"{Observation of the decay B0 ---\\ensuremath{>} J/psi eta}\",\n    eprint = \"hep-ex/0609047\",\n    archivePrefix = \"arXiv\",\n    reportNumber = \"BELLE-CONF-0676\",\n    doi = \"10.1103/PhysRevLett.98.131803\",\n    journal = \"Phys. Rev. Lett.\",\n    volume = \"98\",\n    pages = \"131803\",\n    year = \"2007\"\n}\n"
    }

### Check of Bibtex Entries

The command `hflav_overview` generates the files `overview.tex` and `overview.bib` with a list of all publications and their bibtex entries.
One or more experiment names can be given as arguments to limit the list to those.


## <a name="measurements"></a> Measurements

A measurement can have the following attributes:

* `result` (string): Measurement central value with uncertainties or limit. The format is described [below](#expression).
* `inputs` (array of strings, optional): Factors that are used in the calculation of the result. The factors can be any formula with (external) parameters, e.g. `1 / BR_...` in case of a daughter branching fraction. The parameters must be given with their revision, separated by a colon. A `+` with space in front of a factor indicates that the uncertainty coming from this factor is not yet included in the result. In the fit the central value and uncertainty of the result are adjusted for the change of the input from the previous revision to the current value and uncertainty.
* `nll` (string, required if `result` is not given): Formula of negative log likelihood function of the external parameter. The formula will be evaluated in python.
* `comment` (string or list of strings, optional): A literal comment or an identifier of a comment that will be displayed as a footnote. Multiple comments can be given in a list.
* `superseded` (bool, optional): If True the measurement is considered superseded and will not be included in the fit.
* `color` (string, optional): Forces the use of the given color in the output. If set to `false` no color will be used.

### <a name="expressions"></a> Expressions Format

The `result` of a measurement and an entry in the `inputs` of a measurement has the following form

> `formula_of_parameters` = `central_value` [`stat_error` [`syst_error` [`further_errors`(`nuisance_parameter`)]]] [e`N`]

where

* `formula_of_parameters` is a single parameter name or a formula containing multiple parameter names. The formula will be evaluated in python.
* `central_value` is a numerical value.
* Symmetric errors are given as `+-X`, asymmetric ones as `+X-Y`. Note that the positive one has to be given first. And make sure that the number of digits is consistent for the central value and all errors. Don't drop trailing zeros.
* `nuisance_parameter` is the name of a nuisance parameter given in parentheses after a symmetric error. This can be used for correlated uncertainties.
* e followed by an integer number `N` can be used to indicate that the numbers are given in units of 10<sup>`N`</sup>.

If a `result` is a limit it is given in the form

> `formula_of_parameters` < `value`[e`N`] @ `X`% CL

where `X` is the confidence level.


Example of `measurements` in a publications json file:

    "measurements": [
        {
            "result": "BR_B0bar_pi-_pi+_Jpsi < 1.2 e-5 @ 90% CL"
        },
        {
            "result": "BR_B+_D0_K+ = 3.69 +-0.16 +-0.05 e-3",
            "inputs": [
                "1 / BR_D_K_pi:PDG08" # this label has to correspond to a dictionary key in the 'revisions' field of the other parameter
            ]
        },
        {
            "nll": "0.5 * ((BR_X_Y_Z - 0.37) / 0.08)**2"
        },
        {
            "result": "BR_Lambdab0_p_K-_chic1 / BR_Lambdab0_p_K-_Jpsi = 0.242 +-0.014 +-0.013 +-0.009(chi_c_to_Jpsi_gamma_BF)",
            "comment": "chi_c_BR"
        },
        {
            "result": "BR_Lambdab0_p_K-_chic2 / BR_Lambdab0_p_K-_Jpsi = 0.248 +-0.02 +-0.014 +-0.009(chi_c_to_Jpsi_gamma_BF)",
            "comment": "chi_c_BR"
    }

The first one is a limit on a parameter and the second one a measurement of a parameter that used a branching fraction of the D daughter, identified by the external parameter `BR_D_K_pi`, as input.
The third one defines a negative log likelihood that is equivalent to the result `"BR_X_Y_Z = 0.37 +-0.08"`.
The last two entries are measurements of the ratio of two parameters.
The correlation between the ratios is modeled by the nuisance parameter `chi_c_to_Jpsi_gamma_BF`.
The comments use the identifier `chi_c_BR` defined in the example in the next section.

## <a name="comments"></a> Comments

If the same comment is repeated for multiple measurements, publications, or parameters it can be defined in the `comments.json` file.
The format is a dictionary of comment identifiers to literal comments.

Example json file:

    {
        "chi_c_BR": "The third uncertainty is due to the uncertainty on the branching fractions of the $\\chi_{c1}\\to J/\\psi\\gamma$ and $\\chi_{c2}\\to J/\\psi\\gamma$ decays."
    }

### Automatic footnotes

Footnotes are automatically added to latex tables in the following cases

* An average input value is calculated from a ratio or product of parameters.
The other parameter name is given.
* An average input depends in a more complicated way on the parameter of interest.
The expression is given.
* A measurement is corrected for updates of input parameters.
* Multiple systematic uncertainties are added.
The footnote can be customized by setting the text of the `syst_sum` comment identifier.
Setting it to `false` disables this footnote.
* A confidence level of a limit is not 90%.
* If the comment identifier `caption_` followed by a feature name is defined and the table has the corresponding feature.
Possible features are `red` for a measurement shown in red, `blue` for a measurement shown in blue, `limit` if a limit is shown, `pdg` if a PDG average is shown, and `p-value` if a p-value is shown.

## <a name="experiments"></a> Experiments

Plot colors for several experiments are pre-defined.
The colors and the latex display of experiments can be adjusted by specifying them in the file `experiments.json`.
The file can also contain aliases that will be mapped to the experiment name.

Example json file:

    {
        "MyExp": {"color": "blue", "latex": "$\\frac{My}{Exp}$", "aliases": ["myexp", "myExp"]}
    }

## <a name="structure"></a> Output Structure

The hierarchical structure of sections in the latex or html output is specified in a json file with the following attributes:

* `label` (string): An identifier used for example for latex labels or file names.
* `caption` (string): A caption of a section or page or table. Latex can be used.
* `subsections` (array of sections, optional): Each subsection has again the same format as described here.
* `parameters` (array of strings, optional): The listed parameters will be included in the same table or plot.
The number of displayed digits of an average can be controlled by added `:` after the parameter name and then either `.N` to show `N` digits after the dot or `N` to show digits up to 10^`N`.
To reduce the width of long rows the parameter name can be displayed in a separate row in latex tables. The framework estimates the width of rows to decide when to use a separate row. One can force the use of one or two rows by adding a `s` (for short) or `l` (for long) after the `:` and before any digit specifier.
* `unit` (string, optional): The unit that will be used for all parameter values in the section.
* `options` (array of strings, optional): If an option has an argument it is separated from the option name by a colon.
The option `nopdg` prevents showing any PDG averages.
The option `resize` can be used to adjust the width of a table to the textwidth.
An optional argument can be used to specify a fraction of the textwidth.
Custom latex code before the footnotes can be inserted with the `footnoteprefix` option, e.g. to change the footnote font size.

Example json file:

    {
        "label": "Bd",
        "caption": "$B^0$ decays",
        "subsections": [
            {
                "label": "D",
                "caption": "$B^0$ decays to single charm mesons",
                "subsections": [
                    {
                        "label": "overview1",
                        "unit": "1e-3",
                        "caption": "Branching fractions to a $D^{(*)}$ meson and one or more pions",
                        "parameters": [
                            "BR_B0_D-_pi+:.3",
                            "BR_B0_D*-_pi+",
                            "BR_B0_D*+_pi+_pi-_pi-:l"
                        ]
                    },
                    {
                        "label": "overview2",
                        "caption": "Branching fractions to a $D^{(*)}$ meson and a kaon",
                        "options": ["footnoteprefix:\\small"],
                        "parameters": [
                            "BR_B0_D-_K+",
                            "BR_B0_D*-_K+"
                        ]
                    }
                ]
            }
        ]
    }
 