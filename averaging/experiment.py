#!/usr/bin/env python3
# -*- coding: utf-8 -*-


experiments = {
    'ARGUS': {'color': 'darkcyan'},
    'ATLAS': {'color': 'black'},
    'BaBar': {'color': 'green'},
    'Belle': {'color': 'blue'},
    'Belle II': {'color': 'mediumblue', 'aliases': ['Belle-II']},
    'CDF': {'color': 'red'},
    'CLEO': {'color': 'olive'},
    'CMS': {'color': 'orange'},
    'D0': {'color': 'magenta'},
    'DELPHI': {'color': 'pink'},
    'LHCb': {'color': 'cyan'},
    'SLD': {'color': 'plum'},
}

def update_experiments(update):
    """Update display information of experiments"""

    global experiments
    for name, data in update.items():
        experiments[name] = data


def experiment_name(experiment):
    """Return name of experiment taking into account possible aliases"""

    global experiments
    if experiment not in experiments.keys():
        for name, data in experiments.items():
            if experiment in data.get('aliases', []):
                return name
    return experiment


def experiment_latex(experiment):
    """Return name of experiment in latex format"""

    global experiments
    if experiment in experiments.keys():
        return experiments[experiment].get('latex', experiment)
    else:
        return experiment


def experiment_color(experiment):
    """Return color (for matplotlib) assigned to experiment"""

    global experiments
    if experiment in experiments.keys() and 'color' in experiments[experiment].keys():
        return experiments[experiment]['color']
    else:
        return '#' + hex(hash(experiment))[-6:]
