#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import json


particles = [
    # (string used in parameter names, latex code, identifier for PDG link, particle type filter name for web page)  # mass in MeV (just for ordering of particles in this list)
    ('Bc+', 'B_c^+', 'S091', 'Bc'),  # 6275
    ('Omegab-', '\\Omega_b^-', 'S063', 'Omegab'),  # 6046
    ('Xib-', '\\Xi_b^-', 'S060', 'Xib'),  # 5797
    ('Xib0', '\\Xi_b^0', 'S060', 'Xi_'),  # 5792
    ('Lambdab0', '\\Lambda_b^0', 'S040', 'Lambdab'),  # 5620
    ('Bs0', 'B_s^0', 'S086', 'Bs'),  # 5367
    ('Badmix', 'B', 'S049', 'B'),  # 5280
    ('B+', 'B^+', 'S041', 'Bu'),  # 5279
    ('B0', 'B^0', 'S042', 'Bd'),  # 5280
    ('psi(4660)', '\\psi(4660)', 'M189', 'ccbar'),  # 4633
    ('Yeta', 'Y_\\eta', '???', 'XYZ'),  # 4613
    ('Zc(4430)+', 'Z_c(4430)^+', 'M195', 'XYZ'),  # 4478
    ('Pc(4457)+', 'P_c(4457)^+', 'B172', 'Pc'),  # 4457
    ('Pc(4380)+', 'P_c(4380)^+', 'B171', 'Pc'),  # 4380
    ('psi(4260)', '\\psi(4260)', 'M074', 'ccbar'),  # 4260?
    ('X(4250)+', 'X(4250)^+', 'M192', 'XYZ'),  # 4248
    ('Zc(4200)+', 'Z_c(4200)^+', 'M231', 'XYZ'),  # 4196
    ('chic1(4274)', '\\chi_{c1}(4274)', 'M233', 'ccbar'),  # 4274
    ('chic1(4140)', '\\chi_{c1}(4140)', 'M193', 'ccbar'),  # 4147
    ('X(4050)+', 'X(4050)^+', 'M191', 'XYZ'),  # 4051
    ('Y(3940)', 'Y(3940)', '???', 'XYZ'),  # 3940
    ('X(3915)', 'X(3915)', 'M159', 'XYZ'),  # 3918
    ('Zc(3900)+', 'Z_c(3900)^+', '???', 'XYZ'),  # 3900
    ('X(3872)+', 'X(3872)^+', '???', 'XYZ'),  # 3872
    ('X(3872)', 'X(3872)', 'M176', 'X3872'),  # 3872
    ('psi2(3823)', '\\psi_2(3823)', 'M212', 'ccbar'),  # 3822
    ('psi(3770)', '\\psi(3770)', 'M053', 'ccbar'),  # 3774
    ('psi2S', '\\psi(2S)', 'M071', 'psi2S'),  # 3686
    ('etac2S', '\\eta_c(2S)', 'M059', 'ccbar'),  # 3638
    ('chic2', '\\chi_{c2}', 'M057', 'chic'),  # 3556
    ('hc', 'h_c', 'M144', 'ccbar'),  # 3525
    ('chic1', '\\chi_{c1}', 'M055', 'chic'),  # 3511
    ('chic0', '\\chi_{c0}', 'M056', 'chic'),  # 3415
    ('Jpsi', 'J/\\psi', 'M070', 'Jpsi'),  # 3097
    ('etac', '\\eta_c', 'M026', 'etac'),  # 2984
    ('Xic0(2930)', '\\Xi_c(2930)^+', 'B156', 'cbaryon'),  # 2942
    ('Sigmac(2800)++', '\\Sigma_c(2800)^{++}', 'B155', 'cbaryon'),  # 2801
    ('Sigmac(2800)+', '\\Sigma_c(2800)^+', 'B155', 'cbaryon'),  # 2792
    ('Sigmac(2800)0', '\\Sigma_c(2800)^0', 'B155', 'cbaryon'),  # 2806
    ('Lambdac(2940)+', '\\Lambda_c(2940)^+', 'B122', 'cbaryon'),  # 2940
    ('Lambdac(2880)+', '\\Lambda_c(2880)^+', 'B151', 'cbaryon'),  # 2882
    ('Lambdac(2860)+', '\\Lambda_c(2860)^+', 'B178', 'cbaryon'),  # 2856
    ('Lambdac(2765)+', '\\Lambda_c(2765)^+', 'B150', 'cbaryon'),  # 2767
    ('Lambdac(2625)+', '\\Lambda_c(2625)^+', 'B102', 'cbaryon'),  # 2628
    ('Lambdac(2595)+', '\\Lambda_c(2595)^+', 'B119', 'cbaryon'),  # 2592
    ('Ds1*(2860)+', 'D_{s1}^*(2860)^+', 'M196', 'Ds**'),  # 2859
    ('Ds1*(2700)+', 'D_{s1}^*(2700)^+', 'M182', 'Ds**'),  # 2714
    ('Ds2*(2573)+', 'D_{s2}^*(2573)^+', 'M148', 'Ds**'),  # 2569
    ('Ds1(2536)+', 'D_{s1}(2536)^+', 'M121', 'Ds**'),  # 2535
    ('Ds1(2460)+', 'D_{s1}(2460)^+', 'M173', 'Ds**'),  # 2460
    ('Ds0*(2317)+', 'D_{s0}^*(2317)^+', 'M172', 'Ds**'),  # 2318
    ('Xic+', '\\Xi_c^+', 'S045', 'Xic'),  # 2468
    ('Xic0', '\\Xi_c^0', 'S048', 'Xic'),  # 2471
    ('D1(2430)0', 'D_1(2430)^0', 'M180', 'Ds**'),  # 2427
    ('D2*+', 'D_2^*(2460)^+', 'M150', 'D**'),  # 2465
    ('D1+', 'D_1(2420)^+', 'M120', 'D**'),  # 2423
    ('D0*+', 'D_0^*(2300)^+', 'M179', 'D**'),  # 2349
    ('D2*0', 'D_2^*(2460)^0', 'M119', 'D**'),  # 2461
    ('D10', 'D_1(2420)^0', 'M097', 'D**'),  # 2421
    ('D0*0', 'D_0^*(2300)^0', 'M178', 'D**'),  # 2300
    ('Sigmac*++', '\\Sigma_c(2520)^{++}', 'B115', 'Sigmac'),  # 2518
    ('Sigmac*+', '\\Sigma_c(2520)^+', 'B115', 'Sigmac'),  # 2518
    ('Sigmac*0', '\\Sigma_c(2520)^0', 'B115', 'Sigmac'),  # 2518
    ('Sigmac++', '\\Sigma_c(2455)^{++}', 'B104', 'Sigmac'),  # 2454
    ('Sigmac+', '\\Sigma_c(2455)^+', 'B104', 'Sigmac'),  # 2453
    ('Sigmac0', '\\Sigma_c(2455)^0', 'B104', 'Sigmac'),  # 2454
    ('Lambdac+', '\\Lambda_c^+', 'S033', 'Lambdac'),  # 2286
    ('fJ(2220)', 'f_J(2220)', 'M082', 'light'),  # 2231
    ('Ds*+', 'D_s^{*+}', 'S074', 'Ds*'),  # 2112
    ('K4*(2045)+', 'K_4^*(2045)^+', 'M035', 'kaon'),  # 2048
    ('K4*(2045)0', 'K_4^*(2045)^0', 'M035', 'kaon'),  # 2048
    ('f2(2010)', 'f_2(2010)', 'M106', 'light'),  # 2011
    ('D*+', 'D^*(2010)^+', 'M062', 'D*+'),  # 2010
    ('D*0', 'D^*(2007)^0', 'M061', 'D*0'),  # 2007
    ('Ds+', 'D_s^+', 'S034', 'Ds'),  # 1968
    ('D+', 'D^+', 'S031', 'D+'),  # 1870
    ('D0', 'D^0', 'S032', 'D0'),  # 1864
    ('tau-', '\\tau^-', 'S035', 'tau'),  # 1777
    ('K0*(1950)0', 'K_0^*(1950)^0', 'M134', 'kaon'),  # 1945
    ('K0*(1950)+', 'K_0^*(1950)^+', 'M134', 'kaon'),  # 1945
    ('K2(1820)0', 'K_2(1820)^0', 'M146', 'kaon'),  # 1819
    ('K2(1820)+', 'K_2(1820)^+', 'M146', 'kaon'),  # 1819
    ('K3*(1780)+', 'K_3^*(1780)^+', 'M060', 'kaon'),  # 1776
    ('K3*(1780)0', 'K_3^*(1780)^0', 'M060', 'kaon'),  # 1776
    ('K2(1770)+', 'K_2(1770)^+', 'M023', 'kaon'),  # 1773
    ('K2(1770)', 'K_2(1770)', 'M023', 'kaon'),  # 1773
    ('K*(1680)0', 'K^*(1680)^0', 'M095', 'kaon'),  # 1718
    ('K*(1680)+', 'K^*(1680)^+', 'M095', 'kaon'),  # 1718
    ('f0(1710)', 'f_0(1710)', 'M068', 'light'),  # 1704
    ('phi(1680)', '\\phi(1680)', 'M067', 'phi'),  # 1680
    ('Omega-', '\\Omega^-', 'S024', 'Omega'),  # 1672
    ('rho(1700)+', '\\rho(1700)^+', 'M065', 'light'),  # 1700
    ('rho(1700)0', '\\rho(1700)^0', 'M065', 'light'),  # 1700
    ('rho3(1690)+', '\\rho_3(1690)^+', 'M015', 'light'),  # 1689
    ('rho3(1690)0', '\\rho_3(1690)^0', 'M015', 'light'),  # 1689
    ('Lambda(1520)', '\\Lambda(1520)', 'B038', 'baryon'),  # 1520
    ('f2prime(1525)', 'f_2^{\\prime}(1525)', 'M013', 'light'),  # 1517
    ('f0(1500)', 'f_0(1500)', 'M152', 'light'),  # 1507
    ('eta(1475)', '\\eta(1475)', 'M175', 'light'),  # 1475
    ('rho(1450)+', '\\rho(1450)^+', 'M105', 'light'),  # 1425
    ('rho(1450)0', '\\rho(1450)^0', 'M105', 'light'),  # 1425
    ('a0(1450)+', 'a_0(1450)^+', 'M149', 'light'),  # 1450
    ('K2*(1430)+', 'K_2^*(1430)^+', 'M022', 'kaon'),  # 1432
    ('K2*(1430)0', 'K_2^*(1430)^0', 'M022', 'kaon'),  # 1432
    ('f1(1420)', 'f_1(1420)', 'M006', 'light'),  # 1426
    ('(Kpi)0*+', '(K\\pi)^{*+}_{0}', '???', 'kaon'),  # 1425
    ('(Kpi)0*0', '(K\\pi)^{*0}_{0}', '???', 'kaon'),  # 1425
    ('K0*(1430)+', 'K_0^*(1430)^+', 'M019', 'kaon'),  # 1425
    ('K0*(1430)0', 'K_0^*(1430)^0', 'M019', 'kaon'),  # 1425
    ('K*(1410)+', 'K^*(1410)^+', 'M094', 'kaon'),  # 1414
    ('K*(1410)0', 'K^*(1410)^0', 'M094', 'kaon'),  # 1414
    ('eta(1405)', '\\eta(1405)', 'M027', 'light'),  # 1295
    ('K1(1400)+', 'K_1(1400)^+', 'M064', 'kaon'),  # 1403
    ('K1(1400)0', 'K_1(1400)^0', 'M064', 'kaon'),  # 1403
    ('Sigma(1385)-', '\\Sigma(1385)^-', 'B043', 'baryon'),  # 1387
    ('Sigma(1385)0', '\\Sigma(1385)^0', 'B043', 'baryon'),  # 1384
    ('Sigma(1385)+', '\\Sigma(1385)^+', 'B043', 'baryon'),  # 1383
    ('f0(1370)', 'f_0(1370)', 'M147', 'light'),  # 1200-1500
    ('Xi-', '\\Xi^-', 'S022', 'Xi'),  # 1322
    ('a2+', 'a_2(1320)^+', 'M012', 'light'), # 1318
    ('a20', 'a_2(1320)^0', 'M012', 'light'), # 1318
    ('Xi0', '\\Xi^0', 'S023', 'Xi'),  # 1315
    ('eta(1295)', '\\eta(1295)', 'M037', 'light'),  # 1295
    ('f1', 'f_1(1285)', 'M008', 'light'),  # 1282
    ('f2', 'f_2(1270)', 'M005', 'light'),  # 1276
    ('K1(1270)+', 'K_1(1270)^+', 'M028', 'kaon'),  # 1253
    ('K1(1270)0', 'K_1(1270)^0', 'M028', 'kaon'),  # 1253
    ('b1+', 'b_1(1235)^+', 'M011', 'light'), # 1235
    ('b10', 'b_1(1235)^0', 'M011', 'light'), # 1235
    ('Delta+', '\\Delta(1232)^+', 'B033', 'baryon'), # 1232
    ('Delta0', '\\Delta(1232)^0', 'B033', 'baryon'), # 1232
    ('a1+', 'a_1(1260)^+', 'M010', 'light'), # 1230
    ('a10', 'a_1(1260)^0', 'M010', 'light'), # 1230
    ('Sigma+', '\\Sigma^+', 'S019', 'Sigma'),  # 1189
    ('Sigma-', '\\Sigma^-', 'S020', 'Sigma'),  # 1197
    ('Sigma0', '\\Sigma^0', 'S021', 'Sigma'),  # 1193
    ('Lambda0', '\\Lambda^0', 'S018', 'Lambda'),  # 1116
    ('phi', '\\phi(1020)', 'M004', 'phi'),  # 1019
    ('f0', 'f_0(980)', 'M003', 'light'),  # 990
    ('a0+', 'a_0(980)^+', 'M036', 'light'),  # 980
    ('a00', 'a_0(980)^0', 'M036', 'light'),  # 980
    ('etaprime', '\\eta^\\prime', 'M002', 'eta'),  # 958
    ('p', 'p', 'S016', 'p'),  # 938
    ('n', 'n', 'S017', 'n'),  # 940
    ('K*+', 'K^*(892)^+', 'M018', 'K*+'),  # 896
    ('K*0', 'K^*(892)^0', 'M018', 'K*0'),  # 896
    ('omega', '\\omega(782)', 'M001', 'omega'),  # 783
    ('rho+', '\\rho^+(770)', 'M009', 'rho+'),  # 775
    ('rho0', '\\rho^0(770)', 'M009', 'rho0'),  # 775
    ('eta', '\\eta', 'S014', 'eta'),  # 548
    ('K+', 'K^+', 'S010', 'K'),  # 494
    ('K0', 'K^0', 'S011', 'K0'),  # 498
    ('K0S', 'K^0_S', 'S012', 'K0'),  # 498
    ('K0L', 'K^0_L', 'S013', 'K0'),  # 498
    ('f0(500)', 'f_0(500)', 'M014'),  # 400-550
    ('pi+', '\\pi^+', 'S008', 'pi'),  # 140
    ('pi0', '\\pi^0', 'S009', 'pi0'),  # 134
    ('s', 's', 'Q123'),  # 93 (s quark)
    ('mu-', '\\mu^-', 'S004', 'mu'),  # 106
    ('e-', 'e^-', 'S003', 'e'),  # 0.5
    ('l-', '\\ell^-', None, 'l'),  # negative lepton
    ('nu', '\\nu', 'S066', 'nu'),  # 0
    ('nul', '\\nu_{\\ell}', 'S066', 'nu'),  # 0
    ('nutau', '\\nu_{\\tau}', 'S066', 'nu'),  # 0
    ('numu', '\\nu_{\\mu}', 'S066', 'nu'),  # 0
    ('nue', '\\nu_{e}', 'S066', 'nu'),  # 0
    ('gamma', '\\gamma', 'S000', 'photon'),  # 0
]

# add anti particles to the list
for index in range(len(particles)-1, -1, -1):
    particle = particles[index]
    antiparticle = particle[0]
    latex = particle[1]
    filter = None if len(particle) < 4 else particle[3]
    if '+' in antiparticle:
        antiparticle = antiparticle.replace('+', '-')
        latex = latex.replace('+', '-')
    elif '-' in antiparticle:
        antiparticle = antiparticle.replace('-', '+')
        latex = latex.replace('-', '+')
    # do we need a bar?
    if antiparticle[0] in 'LOPSn' or antiparticle.startswith('Xi') or (antiparticle[0] in 'BDK' and antiparticle[-1] == '0') or antiparticle in ['p']:
        antiparticle += 'bar'
        latex = '\\bar{' + latex
        # close the bracket before _ or ^
        pos = min(latex.find('_'), latex.find('^'))
        if pos < 0:
            pos = max(latex.find('_'), latex.find('^'))
        if pos > 0:
            latex = latex[:pos] + '}' + latex[pos:]
        else:
            latex += '}'
    if antiparticle != particle[0]:
        particles.insert(index+1, (antiparticle, latex, particle[2], filter))


# add special "names"
particles.append(('K*', 'K^*', None, 'K*+'))
particles.append(('K', 'K', None, 'K'))
particles.append(('pi', '\\pi', None, 'pi'))
particles.append(('cc', '\\mathrm{+c.c.}', None))
particles.append(('NR', '\\mathrm{(NR)}', None))
particles.append(('plus', ' + ', None))


particle_indices = {}
for index in range(len(particles)):
    particle_indices[particles[index][0]] = index


particle_filters = {}
for index in range(len(particles)):
    if len(particles[index]) > 3:
        filter = particles[index][3]
        if filter not in particle_filters.keys():
            particle_filters[filter] = []
        particle_filters[filter].append(index)

particle_filter_names = {
    'BR': 'Branching fraction',
    'ACP': 'CP asymmetry',
    'polarization': 'Polarization',
    'm': 'Mass',
    'Bd': '$B^0$',
    'Bu': '$B^+$',
    'B': '$B^0+B^+$',
    'Bs': '$B_s^0$',
    'Bc': '$B_c^+$',
    'Lambdab': '$\\Lambda_b$',
    'Xib': '$\\Xi_b$',
    'Omegab': '$\\Omega_b$',
    'D0': '$D^0$',
    'D+': '$D^\\pm$',
    'Ds': '$D_s^\\pm$',
    'D*0': '$D^{*0}$',
    'D*+': '$D^{*\\pm}$',
    'Ds*': '$D_s^{*\\pm}$',
    'D**': 'excited $D$',
    'Ds**': 'excited $D_s$',
    'Jpsi': '$J/\\psi$',
    'psi2S': '$\\psi(2S)$',
    'etac': '$\\eta_c$',
    'chic': '$\\chi_c$',
    'ccbar': 'other charmonium',
    'X3872': '$X(3872)$',
    'Pc': '$P_c$',
    'XYZ': '$XYZ$ states',
    'p': '$p$',
    'n': '$n$',
    'Lambda': '$\\Lambda$',
    'Sigma': '$\\Sigma$',
    'Xi': '$\\Xi$',
    'Omega': '$\\Omega$',
    'baryon': 'other light baryons',
    'Lambdac': '$\\Lambda_c$',
    'Sigmac': '$\\Sigma_c$',
    'Xic': '$\\Xi_c$',
    'cbaryon': 'other charm baryons',
    'K': '$K^\\pm$',
    'K0': '$K^0_{(S/L)}$',
    'K*+': '$K^{*\\pm}$',
    'K*0': '$K^{*0}$',
    'phi': '$\\phi$',
    'kaon': 'other kaons',
    'pi': '$\\pi^\\pm$',
    'pi0': '$\\pi^0$',
    'rho+': '$\\rho^\\pm$',
    'rho0': '$\\rho^0$',
    'eta': '$\\eta^{(\\prime)}$',
    'omega': '$\\omega$',
    'light': 'other light mesons',
    'e': '$e$',
    'mu': '$\\mu$',
    'tau': '$\\tau$',
    'l': '$\\ell$',
    'nu': '$\\nu$',
    'photon': '$\\gamma$',
}

particle_categories = {
    'Type of observable': ['BR', 'ACP', 'polarization'],
    'Initial particle': ['Bd', 'Bu', 'B', 'Bs', 'Bc', 'Lambdab', 'Xib', 'Omegab'],
    'Charm mesons': ['D0', 'D+', 'Ds', 'D*0', 'D*+', 'Ds*', 'D**', 'Ds**'],
    'Charmonium and exotic states': ['Jpsi', 'psi2S', 'etac', 'chic', 'ccbar', 'X3872', 'Pc', 'XYZ'],
    'Baryons': ['p', 'n', 'Lambda', 'Sigma', 'Xi', 'Omega', 'baryon', 'Lambdac', 'Sigmac', 'Xic', 'cbaryon'],
    'Strange mesons': ['K', 'K0', 'K*+', 'K*0', 'phi', 'kaon'],
    'Light mesons': ['pi', 'pi0', 'rho+', 'rho0', 'eta', 'omega', 'light'],
    'Leptons and photon': ['e', 'mu', 'tau', 'l', 'nu', 'photon'],
}


def merge_filters(merged_filters):
    """combine multiple particle type filter names into one with a new name"""

    global particle_filters, particle_filter_names
    for merged in merged_filters:
        name = merged['name']
        particle_filters[name] = []
        for filter in merged['filters']:
            for particle in particles:
                if len(particle) > 3 and particle[3] == filter:
                    index = particles.index(particle)
                    particles[index] = (particle[0], particle[1], particle[2], name)
                    particle_filters[name].append(index)
            del particle_filters[filter]
        particle_filter_names[name] = merged['latex']


def update_categories(categories):
    """Set new particle categories"""

    global particle_categories
    new_categories = {k:v for k,v in particle_categories.items() if k in ['Type of observable', 'Initial particle']}
    for category, filters in categories.items():
        new_categories[category] = filters
    particle_categories = new_categories


def update_particles(filename):
    """Read and apply changes of particle type filters and categories"""

    try:
        with open(filename) as json_file:
            updates = json.loads(json_file.read())
            if 'merged_filters' in updates.keys():
                merge_filters(updates['merged_filters'])
            if 'categories' in updates.keys():
                update_categories(updates['categories'])
    except Exception as exception:
        print(f'ERROR: Update of particles with {filename} failed')
        print(exception)

if os.path.exists('particles.json'):
    update_particles('particles.json')


def generate_html():
    """Print a html table of particles"""

    print('<table>\n<tr><th>name</th><th>latex</th><th>PDG ID</th></tr>')
    for particle in particles:
        pdg_link = f'<a href="https://pdglive.lbl.gov/Particle.action?node={particle[2]}">{particle[2]}</a>' if particle[2] is not None else ''
        print(f'<tr><td>{particle[0]}</td> <td>{particle[1]}</td> <td>{pdg_link}</td></tr>')
    print('</table>')

