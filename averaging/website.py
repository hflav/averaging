#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .display import DisplayData
import os
from django.core.management.utils import get_random_secret_key
from django.core.management import execute_from_command_line


class WebSite:
    """Class to create website code and run it."""

    def __init__(self, averaging):
        """Create configuration files if they don't exists yet."""

        dir = 'website'
        if not os.path.exists(dir):
            try:
                os.mkdir(dir)
            except OSError:
                print(f'ERROR: could not create directory {dir}')
                return
            if not os.path.exists('static/user_gen_plots'):
                os.makedirs('static/user_gen_plots')
            open(os.path.join(dir, '__init__.py'), 'w').close()
            with open(os.path.join(dir, 'settings.py'), 'w') as settings:
                settings.write(f"SECRET_KEY = '{get_random_secret_key()}'\n")
                settings.write(f"TITLE = '{averaging.config.get('web_title')}'\n")
                settings.write(f"PLOT_SUBTITLE = '{averaging.config.get('plot_subtitle')}'\n")
                settings.write("""
from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent
DEBUG = True
ALLOWED_HOSTS = ['*']
INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'averaging',
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
ROOT_URLCONF = 'website.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
WSGI_APPLICATION = 'website.wsgi.application'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    BASE_DIR / 'static',
]
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
""")
            with open(os.path.join(dir, 'urls.py'), 'w') as urls:
                urls.write("""
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns = [
    path('', include('averaging.urls'))
]
urlpatterns += staticfiles_urlpatterns()
""")
            with open(os.path.join(dir, 'wsgi.py'), 'w') as wsgi:
                wsgi.write("""
import os
from django.core.wsgi import get_wsgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
application = get_wsgi_application()
""")

        database = 'db.sqlite3'
        if os.path.exists(database):
            os.remove(database)

        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
        execute_from_command_line(['manage.py', 'migrate'])

        data = DisplayData(averaging)
        from .models import ParameterData
        for parameter in data.parameters.values():
            ParameterData(data=parameter).save()
