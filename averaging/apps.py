from django.apps import AppConfig


class AveragingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'averaging'
