#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy
from hashlib import sha256
from .parser import parse_formula, parse_expression
from .error import Error, errorToList
import math


class Input:
    """Input factor use in a measurement"""

    def __init__(self, text, parameters, used_parameters):
        """Initialize the input by parsing the given text."""

        self.text = text
        self.formula, self.revisions = parse_formula(text, parameters, used_parameters)
        self.additional = self.formula.startswith('+ ')
        if self.additional:
            self.formula = self.formula[2:]

        par = [0] * len(parameters.keys())
        for name, revision in self.revisions.items():
            parameter = parameters[name]
            par[parameter.index] = parameter.revisions[revision].value
        self.value = eval(self.formula)
        self.error = self.get_error(parameters, par)


    def get_error(self, parameters, par):
        """Calculate the error for the given parameters"""

        if len(self.revisions.keys()) == 1 and self.formula == f'par[{parameters[list(self.revisions.keys())[0]].index}]':  # just one input parameter
            name = list(self.revisions.keys())[0]
            error = deepcopy(parameters[name].revisions[self.revisions[name]].error)
        else:
            error = Error(0)
            par_default = deepcopy(par)
            for name, revision in self.revisions.items():
                index = parameters[name].index
                par_input = parameters[name].revisions[revision]
                par = deepcopy(par_default)
                par[index] = par_default[index] + par_input.error.pos
                diff_pos = eval(self.formula) - self.value
                par[index] = par_default[index] + par_input.error.neg
                diff_neg = eval(self.formula) - self.value
                par[index] = par_default[index]
                if diff_pos * diff_neg < 0:
                    error.add(Error(diff_pos, diff_neg))
                else:
                    error.add(Error(max(abs(diff_pos), abs(diff_neg))))
        return error


class Measurement:
    """Representation of a measurement."""

    def __init__(self, dct):
        """Initialize the measurement object from a dictionary."""

        self.comment = None      # a literal comment or an identifier of a comment or an array of those that will be displayed as footnote(s)
        self.superseded = False  # a flag that indicates if the result is superseded
        self.color = None        # a color value used for the display of the measurement, a value of False will disable any color setting
        self.result = None       # a string representation of the measured quantity and its central value and uncertainties
        self.inputs = None       # array of string representations or Input objects of factors used as external input
        self.nll = None          # string with formula of negative log likelihood function

        for attr in self.__dict__:
            if attr in dct.keys():
                self.__setattr__(attr, dct[attr])

        self.publication = None  # publication quoting the measurement
        self.limit = False       # a flag that indicated if this is a limit
        self.value = None        # central value or limit obtained from self.result
        self.precision = None    # precision of central value obtained from self.result
        self.stat_error = None   # statistical error obtained from self.result
        self.syst_error = None   # systematic error obtained from self.result
        self.syst_sum = False    # a flag that indicates if multiple systematic errors are combined
        self.parameters = None   # set of parameters to which the measurement is sensitive
        self.nuisances = None    # set of nuisance parameters to which the measurement is sensitive

        self.corrected_parameters = None  # list of input parameters for which value and error are corrected
        self.corrected_value = None       # central value corrected for updated input parameters
        self.corrected_stat_error = None  # statistical error corrected for updated input parameters
        self.corrected_syst_error = None  # systematic error corrected for updated input parameters


    def hash(self):
        """Calculate hash from publication reference and result or nll string"""

        text = self.publication.link() + (self.result if self.result is not None else self.nll)
        return sha256(text.encode('utf-8')).hexdigest()


    def create_nll(self, parameters, used_parameters, warn_location=None):
        """Create the negative log likelihood function either from the nll string or the result and inputs"""

        if self.nll is None:
            self.result_dict = parse_expression(self.result, parameters, used_parameters, warn_location)
            self.parameters = self.result_dict['parameters']
            self.nuisances = set()
            if 'correlations' in self.result_dict.keys():
                self.nuisances = set(self.result_dict['correlations'].keys())

            if 'limit' in self.result_dict.keys():
                self.value = self.result_dict['limit']
                self.precision = self.result_dict['precision']
                self.limit = True
                for name in list(self.parameters):
                    parameters[name].limits.append(self)
                cl = self.result_dict['cl']
                if cl != 90:
                    if self.comment is None:
                        self.comment = []
                    elif not isinstance(self.comment, list):
                        self.comment = [self.comment]
                    self.comment.append(f'At CL={cl}\\,\\%.')
                return

            x = '(' + self.result_dict['observable'] + ')'

            self.value = self.result_dict['value']
            self.precision = self.result_dict['precision']
            self.stat_error = deepcopy(self.result_dict['stat_error'])
            error = deepcopy(self.stat_error)
            if 'syst_error' in self.result_dict and self.result_dict['syst_error'] is not None:
                self.syst_error = deepcopy(self.result_dict['syst_error'])
                error.add(self.syst_error)
                self.syst_sum = self.result_dict.get('syst_sum', False)
                if self.syst_sum:
                    if self.comment is None:
                        self.comment = []
                    elif not isinstance(self.comment, list):
                        self.comment = [self.comment]
                    self.comment.append('syst_sum')

            if 'correlations' in self.result_dict:
                for name, scale in self.result_dict['correlations'].items():
                    x = f'({x} + {scale}*par[{parameters[name].index}])'
                    if self.syst_error is None:
                        self.syst_error = Error(scale)
                    else:
                        self.syst_error.add(Error(scale))

            inputs = []
            if self.inputs is not None:
                for text in self.inputs:
                    input = Input(text, parameters, used_parameters)
                    inputs.append(input)
                    for name in input.revisions.keys():
                        if name not in self.parameters:
                            self.nuisances.add(name)

                    x += f' * {input.value} / ({input.formula})'
                    if not input.additional:
                        if not error.reduce(input.error * (self.value / input.value)) and warn_location:
                            print(f'WARNING: Subtracted error of input {input.text} larger than given error for {self.result} in {warn_location}.')

            self.inputs = inputs

            if error.symmetric():
                self.nll = f'nll_sym_gauss({x}, {self.value}, {error.pos})'
            else:
                self.nll = f'nll_asym_gauss({x}, {self.value}, {error.pos}, {abs(error.neg)})'

            # sums to determine simple weighted mean of measurements
            if len(self.parameters) == 1:
                parameter = parameters[list(self.parameters)[0]]
                w = error.weight()
                parameter.sumwx += w * self.result_dict['value']
                parameter.sumw += w

            for name in list(self.parameters) + list(self.nuisances):
                parameters[name].measurements.append(self)
                parameters[name].pubs.add(self.publication.inspire)

        else:
            self.nll, self.parameters = parse_formula(self.nll, parameters, used_parameters)
            if set(self.parameters.values()) != {None}:
                raise Exception(f'Unexpected revision of parameter in {self.nll}')


    def get_color(self, parameter):
        """Color for preliminary results or results not included in the PDG average or an explicitly set color"""

        if self.color is None:
            return self.publication.color(parameter)
        elif self.color is False:
            return None
        return self.color


    def determine_corrected(self, parameters, par):
        """Calculate the central value and errors corrected for updated input parameters"""

        self.corrected_parameters = set()
        self.corrected_value = self.value
        self.corrected_stat_error = deepcopy(self.stat_error)
        self.corrected_syst_error = deepcopy(self.syst_error)

        for input in self.inputs:
            for name, revision in input.revisions.items():
                parameter = parameters[name]
                par_input = parameter.revisions[revision]
                if parameter.value != par_input.text:
                    self.corrected_parameters.add(name)
            factor = eval(input.formula) / input.value
            self.corrected_value *= factor
            self.corrected_stat_error.scale(factor)
            scale = self.value / input.value
            if not input.additional:
                if not self.corrected_syst_error.reduce(input.error * (self.value / input.value)):
                    print(f'WARNING: Subtracted error of input {input.text} larger than given error for {self.result}.')
            self.corrected_syst_error.add(input.get_error(parameters, par) * scale)


    def get_contribution(self, parameter, parameters):
        """Determine the contribution of the measurement to the average of the given parameter"""

        entry = {'chi2': parameter.average_inputs[self.hash()]['chi2'] if self.hash() in parameter.average_inputs.keys() else -1}
        value = self.corrected_value or self.value
        precision = self.precision
        stat_error = deepcopy(self.corrected_stat_error or self.stat_error)
        syst_error = deepcopy(self.corrected_syst_error or self.syst_error)
        if len(self.parameters) == 1 and self.result.split()[1] in ['=', '<']:  # only the parameter of interest
            pass
        elif len(self.parameters) == 2 and self.result.startswith(parameter.name + ' ') and self.result.split()[1] in ['*', '/']:  # parameter of interest multiplied or divided by another parameter
            other = parameters[(self.parameters - set([parameter.name])).pop()]
            other_value = other.average_value
            if self.result.split()[1] == '/':
                factor = other_value
                ratio_factor = value
            else:
                if other_value == 0:  # normalization is zero
                    return None
                factor = 1 / other_value
                ratio_factor = value * factor * factor
            value *= factor
            precision *= abs(factor)
            if stat_error:
                stat_error.scale(factor)
            if syst_error:
                syst_error.scale(factor)
            if other.average_error and not self.limit:
                other_error = deepcopy(other.average_error)
                other_error.scale(ratio_factor)
                if syst_error:
                    syst_error.add(other_error)
                else:
                    syst_error = deepcopy(other_error)
        else:  # some more complicated formula with the parameter of interest, no corrected value and errors for now
            value = stat_error = syst_error = None
            latex = self.result_dict['observable'].replace(' * ', ' ')
            for name in self.parameters:
                par = parameters[name]
                latex = latex.replace(f'par[{par.index}]', par.latex if par.latex else par.name)
            entry['latex'] = latex
        entry.update({'value': value, 'precision': precision, 'stat_error': errorToList(stat_error), 'syst_error': errorToList(syst_error), 'corrections': list(self.parameters - set([parameter.name]))})
        if self.corrected_parameters:
            entry['comment'] = 'Corrected for updates of $' + '$, $'.join([parameters[name].latex for name in self.corrected_parameters]) + '$.'
        if value:
            if stat_error:
                error = deepcopy(stat_error)
                if syst_error:
                    error.add(stat_error)
                entry['sort1'] = error.weight()
            else:
                entry['sort1'] = -value
            entry['sort2'] = ''
        else:
            entry['sort1'] = -math.inf
            entry['sort2'] = self.publication.experiment + self.publication.bibtex_id
        return entry
