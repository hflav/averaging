#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import math
import shutil
import subprocess
from .plot import overview_plot, result_plot
from .latex import latex_result, latex_average
from .error import toError
from .experiment import experiment_latex

class HtmlOutput:
    """Class to generate html code of the averaging results"""

    def __init__(self, config = None):
        """Create an output directory for the html code."""

        if config is None:
            config = {}
        dir = config.get('html_dir', 'html')
        if not os.path.exists(dir):
            try:
                os.mkdir(dir)
            except OSError:
                print(f'ERROR: could not create directory {dir}')
                return
        self.dir = dir
        self.css = os.path.exists('style.css')
        if self.css:
            shutil.copyfile('style.css', os.path.join(dir, 'style.css'))
        self.warn_output = config.get('warn_output', False) or config.get('warn_all', False)
        self.debug = config.get('html_debug', False)
        self.nice_captions = config.get('html_nice_captions', False) 


    def create_html(self, levels, caption, html_name = 'index.html'):
        """Write a html header and start of body."""

        filename = os.path.join(self.dir, '/'.join(levels[1:]), html_name)
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        relpath = '../' * (len(levels) - 1)
        css = ''
        if self.css:
            css = f'<link rel="stylesheet" type="text/css" href="{relpath}style.css">\n'
        html = open(filename, 'w')
        html.write(f"""<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>{caption}</title>
<script>MathJax = {{tex: {{inlineMath: [['$', '$']]}}}};</script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
{css}<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<h1>{caption}</h1>
""")
        return html


    def html_comment(self, comments):
        """Return html code for the given comments."""

        if comments is None:
            return ''

        if not isinstance(comments, list):
            comments = [comments]

        result = ''
        for comment in comments:
            if comment in self.averaging.comments.keys():
                comment = self.averaging.comments[comment]
            if comment is False:
                continue
            if comment[-1] != '.':
                comment += '.'
            result += comment + ' '

        return result


    def write_section(self, levels, caption, subsections):
        """Write a section of given level to the main output file."""

        with self.create_html(levels, caption) as html:
            html.write('<ul>\n')
            for subsection in subsections:
                html.write(f'<li><a href="{subsection["label"]}/index.html">{subsection["caption"]}</a></li>\n')
            html.write('</ul>\n</body>\n</html>\n')


    def write_tables(self, levels, caption, subsections):
        """Write a page with tables."""

        with self.create_html(levels, caption) as html:
            for subsection in subsections:
                html.write(f'<h2>{subsection["caption"]}</h2>\n<table>\n')
                for name in subsection['parameters']:
                    if ':' in name:
                        name = name.split(':')[0]
                    if name not in self.averaging.parameters.keys():
                        print(f'WARNING: parameter {name} unknown')
                        continue
                    parameter = self.averaging.parameters[name]
                    html.write(f'<tr><td>{parameter.latex}</td><td>{parameter.average_value}</td></tr>\n')
                html.write('</table>\n')
            html.write('</body>\n</html>\n')


    def write_plots(self, levels, caption, subsections):
        """Write a page with overview plots."""

        with self.create_html(levels, caption) as html:
            for subsection in subsections:
                parameters = []
                for name in self.averaging.filter(subsection['parameters']):
                    if ':' in name:
                        name = name.split(':')[0]
                    if name not in self.averaging.parameters.keys():
                        print(f'WARNING: parameter {name} unknown')
                        continue
                    parameter = self.averaging.parameters[name]
                    if parameter.average_value is None:
                        continue
                    self.write_parameter(levels, parameter, subsection.get('options', None))
                    parameters.append(parameter)
                if parameters:
                    filename = os.path.join(self.dir, '/'.join(levels[1:]), subsection['label'] + '.png')
                    if self.nice_captions: 
                        html.write(f'<h2>{subsection["caption"]}</h2>')
                        map = overview_plot(parameters, filename, '', subsection.get('options', None))
                    else:
                        map = overview_plot(parameters, filename, subsection['caption'], subsection.get('options', None))
                    html.write(f'<img src="{subsection["label"]}.png" usemap="#map_{subsection["label"]}"><br>\n')
                    html.write(map)
            html.write('</body>\n</html>\n')


    def write_parameter(self, levels, parameter, options=None):
        """Write a page with measurements and average of a parameter."""

        if self.warn_output and parameter.name in self.parameters:
            print(f'WARNING: parameter {parameter.name} appears multiple times in html output')
        self.parameters.append(parameter.name)

        result_plot(parameter, self.averaging.measurements, os.path.join(self.dir, '/'.join(levels[1:]), parameter.name + '.png'), options)

        features = set()
        with self.create_html(levels, f'${parameter.latex}$', parameter.name + '.html') as html:
            html.write(f'<p><img src="{parameter.name}.png"></p>\n')

            unit_exp, digits = parameter.get_unit_digits()
            unit = 10 ** unit_exp
            html.write(f'<p><table border="1"><tr><th>Experiment</th><th>Measurement [10<sup>{unit_exp}</sup>]</th><th>$\\Delta\\chi^2$</th><th>Reference</th><th>Comments</th></tr>\n')

            # average
            html.write(f'<tr><td><b>Average</b></td><td><b>${latex_result(unit, digits, parameter.average_value, parameter.average_error)}$</b></td>')
            if parameter.average_ndf > 0:
                html.write(f'<td>{parameter.average_chi2:.2f}</td><td>$p=')
                p_digits = -math.floor(math.log10(abs(parameter.average_p)))
                if p_digits < 4:
                    html.write(f'{parameter.average_p:.{p_digits + 1}f}')
                else:
                    html.write(f'{(10 ** p_digits) * parameter.average_p:.2f} \\times 10^{{{-p_digits}}}')
                html.write(f'$ (ndf={parameter.average_ndf})</td>')
            else:
                html.write('<td></td><td></td>')
            debug = ''
            if self.debug:
                debug = f'{"<br>" if parameter.comment else ""}<font color="gray">DEBUG: <a href=\"{"../" * len(levels) + parameter.filename}\">{parameter.name}</a></font>'
            html.write(f'<td>{self.html_comment(parameter.comment)}{debug}</td></tr>\n')

            # PDG
            if parameter.pdg_value is not None and (options is None or 'nopdg' not in options):
                debug = ''
                if self.debug:
                    debug = f'<font color="gray">DEBUG: <a href="{"../" * len(levels)}pdg/{parameter.pdg_id}.json">{parameter.pdg_id}</a>'
                    if parameter.pdg_diff():
                        debug += f', &nbsp; shift in $\\sigma$: {parameter.pdg_diff():.2f}'
                    if parameter.pdg_error_diff():
                        debug += f', &nbsp; error scale factor: {parameter.pdg_error_diff():.2f}'
                    debug += '</font>'
                    diff = parameter.pdg_pubs.difference(parameter.pubs)
                    if len(diff) > 0:
                        debug += '<br><font color="gray">DEBUG: Publications included in PDG, but not this average: ' + ', '.join([f'<a href="https://inspirehep.net/literature/{id}">{id}</a>' for id in diff]) + '</font>'
                html.write(f'<tr><td><i>PDG</i></td><td><i>${latex_result(unit, digits, parameter.pdg_value, parameter.pdg_error)}$</i></td><td></td><td><a href="{parameter.pdg_link()}">pdgLive</a></td><td>{debug}</td></tr>\n')

            # measurements
            for key, input in parameter.average_inputs.items():
                if key in self.averaging.parameters.keys():
                    continue
                measurement = self.averaging.measurements[key]
                style = ''
                color = measurement.get_color(parameter)
                if color is not None and (options is None or 'nopdg' not in options or color != 'red'):
                    style = f' style="color:{color}"'
                    features.add(color)
                html.write(f'<tr><td{style}>{experiment_latex(measurement.publication.experiment)}</td>')
                if input['value'] is not None:
                    html.write(f'<td{style}>${latex_result(unit, unit_exp - math.floor(math.log10(input["precision"])), input["value"], toError(input.get("stat_error", None)), toError(input.get("syst_error", None)))}$')
                    if len(input.get('corrections', [])) > 0:
                        html.write(' using ' + ', '.join([f'${self.averaging.parameters[name].latex}$' for name in input['corrections']]))
                    html.write('</td>')
                    if input.get("stat_error", None) is None:
                        features.add('limit')
                else:
                    html.write(f'<td{style}>${input["latex"]}$</td>')
                if input['chi2'] >= 0:
                    html.write(f'<td>{float(input["chi2"]):.2f}</td>\n')
                else:
                    html.write(f'<td></td>')
                debug = ''
                if self.debug:
                    debug = f'{"<br>" if measurement.comment else ""}<font color="gray">DEBUG: <a href=\"{"../" * len(levels)}publications/{measurement.publication.experiment}/{measurement.publication.inspire}.json\">{measurement.publication.inspire}</a></font>'
                html.write(f'<td{style}>{measurement.publication.link()}</td><td>{self.html_comment(measurement.comment)}{debug}</td></tr>\n')
            html.write('</table>\n')

            # feature explanation
            if len(features) > 0:
                if 'red' in features:
                    html.write('Input values that appear in red are not included in the PDG average.\n')
                if 'blue' in features:
                    html.write('Input values in blue correspond to yet unpublished results.\n')
                if 'limit' in features:
                    html.write('Quoted upper limits are at 90% confidence level (CL), unless mentioned otherwise.\n')
            html.write('</p>\n')

            # correlations
            entries = []
            for name, correlation in sorted(parameter.average_correlations.items()):
                par = self.averaging.parameters[name]
                if par.type == 'fit':
                    entries.append([par, correlation, None])
            for key, input in parameter.average_inputs.items():
                if key in self.averaging.parameters.keys():
                    entries.append([self.averaging.parameters[key], parameter.average_correlations[key], float(input["chi2"])])
            entries.sort(key=lambda entry: abs(entry[1]), reverse=True)
            order = {'fit': 0, 'external': 1, 'nuisance': 2}
            entries.sort(key=lambda entry: order[entry[0].type])

            if len(entries) > 0:
                dependent_parameters = [parameter.name] + list(parameter.average_correlations.keys())
                basefilename = os.path.join(self.dir, '/'.join(levels[1:]), parameter.name)
                graphfilename = basefilename + '_dependencies.png'
                self.averaging.dependency_graph(dependent_parameters, filename=basefilename + '.dot', highlights=[parameter.name])
                try:
                    subprocess.run(['neato', '-Tpng', '-o', graphfilename, basefilename + '.dot'])
                except:
                    pass
                dependencies = ''
                if os.path.exists(graphfilename):
                    dependencies = f' (<a href=\"{parameter.name}_dependencies.png\">dependency graph</a>)'

                html.write(f'<h3>Further parameters used in the fit and their correlation with the average{dependencies}</h3>\n<p><table border="1"><tr><th>Source</th><th>Parameter</th><th>Correlation [%]</th><th>Value</th><th>$\\Delta\\chi^2$</th></tr>\n')
                for entry in entries:
                    par = entry[0]
                    name = par.name if par.latex is None else f'${par.latex}$'
                    if par.name in self.links.keys():
                        name = f'<a href=\"{"../" * (len(levels)-1)}{self.links[par.name]}\">{name}</a>'
                    chi2 = f'{entry[2]:.2f}' if entry[2] is not None else ''
                    debug = ''
                    if self.debug:
                        debug = f' <font color="gray">DEBUG: <a href=\"{"../" * len(levels) + par.filename}\">{par.name}</a></font>'
                    html.write(f'<tr><td>{par.type}</td><td>{name}</td><td>{100 * entry[1]:.1f}</td><td>${latex_average(par)}$</td><td>{chi2}{debug}</td></tr>\n')
                    features.add(par.type)
                html.write('</table>\n')

                # feature explanation
                if 'fit' in features:
                    html.write('Parameters of interest whose average is determined from individual measurements are called <i>fit</i> parameters.\n')
                if 'external' in features:
                    html.write('Parameters that are needed by the fit (in particular daughter branching fraction) and whose average is not determined here, but taken from somewhere else (usually PDG) are called <i>external</i> parameters.\n')
                if 'nuisance' in features:
                    html.write('Auxiliary fit parameters without direct physical meaning (in particular those to model correlated systematic uncertainties) are called <i>nuisance</i> parameters and constrained by a normally distributed likelihood function.\n')
                html.write('</p>\n')

            html.write('</body>\n</html>\n')


    def determine_links(self, levels, caption, subsections):
        """Create a map of parameter names to links in structure"""

        for subsection in subsections:
            for name in subsection['parameters']:
                name = name.split(':')[0]
                self.links[name] = f'{"/".join(levels[1:])}/{name}.html'


    def traverse(self, structure, levels, parameters_action, section_action):
        """Traverse the given structure and write sub-pages."""

        if 'subsections' not in structure:
            return
        label = structure.get('label', None)
        levels.append(label)
        caption = structure.get('caption', None)
        subsections = structure['subsections']
        if 'parameters' in subsections[0].keys():
            if parameters_action:
                parameters_action(levels, caption, subsections)
        else:
            if section_action:
                section_action(levels, caption, subsections)
            for subsection in subsections:
                self.traverse(subsection, levels + [], parameters_action, section_action)


    def run(self, averaging, structure):
        """Produce html code for the given averages using the given structure.

The structure is a dictionary with the follwing keys
- "label": an identifier for latex labels and file names,
- "caption": a title of the section or table,
- "parameters" (optional): a list of parameter names that will be put into a table
- "subsections" (optional): a list of dictionaries of structures of the next level
"""

        self.averaging = averaging
        self.parameters = []
        self.links = {}
        self.traverse(structure, [], self.determine_links, None)
        self.traverse(structure, [], self.write_plots, self.write_section)
        if self.warn_output:
            for name in self.averaging.parameters.keys():
                parameter = self.averaging.parameters[name]
                if name not in self.parameters and parameter.type == 'fit' and parameter.average_value is not None:
                    print(f'WARNING: parameter {name} not included in html output')
