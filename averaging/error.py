#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math


class Error:
    """Representation of a symmetric or asymmetric uncertainty"""

    def __init__(self, error1, error2=None):
        """Initialize Error. In case of an asymmetric errors both have to be given with a negative sign for the negative one."""

        if isinstance(error1, list) and error2 is None:
            if len(error1) == 2:
                error2 = error1[1]
            error1 = error1[0]
        if error2 is None:
            self.neg = -error1
            self.pos = error1
        else:
            self.neg = min(error1, error2)
            self.pos = max(error1, error2)


    def __eq__(self, error):
        """Check if both errors are equal"""

        return (self.pos == error.pos) and (self.neg == error.neg)


    def __ne__(self, error):
        """Check if both errors are different"""

        return not (self == error)


    def symmetric(self):
        """Return True if we have symmetric errors."""

        return (self.neg + self.pos) == 0


    def add(self, error):
        """Quadratically add the given error."""

        self.neg = -math.sqrt(self.neg*self.neg + error.neg*error.neg)
        self.pos = math.sqrt(self.pos*self.pos + error.pos*error.pos)
        return self


    def __add__(self, error):
        """Quadratically add the errors."""

        result = Error(self.pos, self.neg)
        result.add(error)
        return result


    def subtract(self, error):
        """Quadratically subtract the given error."""

        self.neg = -math.sqrt(self.neg*self.neg - error.neg*error.neg)
        self.pos = math.sqrt(self.pos*self.pos - error.pos*error.pos)
        return self


    def __sub__(self, error):
        """Quadratically subtract the errors."""

        result = Error(self.pos, self.neg)
        result.subtract(error)
        return result


    def reduce(self, error):
        """Set the error to the quadratic difference. Returns False if the given error is larger than this one."""

        result = (self.neg < error.neg) and (self.pos > error.pos)
        self.neg = -math.sqrt(abs(self.neg*self.neg - error.neg*error.neg))
        self.pos = math.sqrt(abs(self.pos*self.pos - error.pos*error.pos))
        return result


    def scale(self, factor):
        """Scale the error by the given factor."""

        self.neg *= abs(factor)
        self.pos *= abs(factor)
        return self


    def __mul__(self, factor):
        """Multiply the error by the given factor."""

        result = Error(self.pos, self.neg)
        result.scale(factor)
        return result


    def __truediv__(self, factor):
        """Divide the error by the given factor."""

        result = Error(self.pos, self.neg)
        result.scale(1. / factor)
        return result


    def weight(self):
        """Average weight = 1 / error^2."""

        return 2 / (self.neg*self.neg + self.pos*self.pos)


    def toList(self):
        """Return list with negative and positive error."""

        return [self.neg, self.pos]


def toError(error):
    """Error object created from error argument."""

    if error is None:
        return None
    return Error(error)


def errorToList(error):
    """List with negative and positive error created from error argument."""

    if error is None:
        return None
    return error.toList()
