#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .error import Error
import re


allowed_characters = re.compile('[\w' + re.escape('-+*().') + ']+')


def parse_formula(text, parameters, used_parameters):
    """Check for parameters names in the text string and replace them by 'par[index]' strings.
    
    parameters is a dictionary of parameter names to Parameter objects.
    used_parameters is a list of parameters names. If the text contains parameters that are not yet in the list they are added and their index data member is set to the index in the list.
    Note that if parameters are used in equations they must be separated from operators (+, -, and *) and parentheses by a space.
    The function returns the parsed text and a dictionary of parameter names that were found in the text to used revision (or None if no revision).
    """

    revisions = {}
    for name, parameter in parameters.items():
        names = [name]
        if parameter.revisions:
            names = [f'{name}:{revision}' for revision in parameter.revisions.keys()] + names
        for pattern in names:
            if len(pattern) <= len(text) and re.search(re.escape(pattern), text) is not None:
                for pos in range(len(text)-len(pattern), -1, -1):
                    if text[pos:pos+len(pattern)] == pattern and (pos == 0 or allowed_characters.match(text[pos-1]) is None) and (pos == len(text)-len(pattern) or allowed_characters.match(text[pos+len(pattern)]) is None):
                        if name not in revisions.keys():
                            revisions[name] = None
                            if parameter.index < 0:
                                parameter.index = len(used_parameters)
                                used_parameters.append(name)
                        index = parameter.index
                        text = text[0:pos] + f'par[{index}]' + text[pos+len(pattern):]
                        if ':' in pattern:
                            revision = pattern.split(':')[1]
                            if revisions[name] is not None and revisions[name] != revision:
                                raise Exception(f'Different revisions, {revisions[name]} and {revision}, used for parameter {name} in the same formula')
                            revisions[name] = revision


    return text, revisions


def parse_number(text, unit = 1):
    """Convert the beginning of the text string to a number or an Error object.

    unit is a factor by which the values are multiplied.
    Symmetric errors are given as "+-X", asymmetric ones as "+X-Y". Note that the positive one has to be given first.
    The function returns the number or Error object, the text after the number/error, and the precision matching the number of given digits.
    """

    if text is None or text == '':
        return (None, None, None)
    is_error = False
    if text[:2] == '+-':
        text = text[2:]
        is_error = True
    has_plus = (text[0] == '+')
    
    pattern = re.compile('^[+-]?[0-9]+(\.[0-9]*)?')
    match = pattern.search(text)
    if match is None:
        return (None, None, None)

    number = match.group()
    digits = 0
    if '.' in number:
        digits = len(number) - number.find('.') - 1

    result = eval(match.group()) * unit
    text = text[match.span()[1]:].strip()
    if has_plus and text[0] == '-':
        match = pattern.search(text)
        result = Error(eval(match.group()) * unit, result)
        text = text[match.span()[1]:].strip()
    elif is_error:
        result = Error(result)

    return (result, text, unit * 10 ** (-1 * digits))


def parse_value(text):
    """Parse a string with a central value and uncertainty.

    The function returns the central value, the Error object, and the precision matching the number of given digits.
    """

    # check for unit
    unit = 1
    match = re.search('(e|E)-?[0-9]+$', text)
    if match is not None:
        unit = eval('1' + match.group())
        text = text[:-len(match.group())]

    # parse central value and error
    value, text, precision = parse_number(text, unit)
    error, text, precision = parse_number(text, unit)

    return (value, error, precision)


def parse_expression(text, parameters, used_parameters, warn_location=None):
    """Parse an expression of the form
    
    [optional_prefix] formula_of_parameters = central_value [stat_error [syst_error [further_errors(nuisance_parameter)]]] [eN]

    or

    formula_of_parameters < value[eN] @ X% CL

    where:
    optional_prefix can be '+' to indicate that the uncertainty of an input parameter is not yet considered in the systematic error of the measurement.
    formula_of_parameters is a single parameter name or a formula containing multiple parameter names, see parse_formula.
    central_value is a numerical value.
    Errors are given in the format explained in parse_number.
    nuisance_parameter is the name of nuisance parameter (with normal pdf).
    e followed by an integer number N can be used indicate that the numbers are given in units of 10^N.
    X is the confidence level.

    parameters is a dictionary of parameter names to Parameter objects.
    used_parameters is a list of parameters names. If the text contains parameters that are not yet in the list they are added and their index data member is set to the index in the list.
    warn_location is included in warning messages about unknown nuisance parameters. No warning is printed if warn_location is None.

    The function returns a dictionary:
    observable is the parsed formula_of_parameters.
    value is the central value
    stat_error is the statistical error
    syst_error is the systematic error
    limit is the value of a limit
    cl is the confidence level of a limit
    parameters is the set of parameters appearing in the text.
    correlations is a dictionary of nuisance parameters to scaling factors.
    additional indicates if the uncertainty of an input parameter should be considered as additional systematic error.ArithmeticError.
    """

    try:
        result = {}

        if '=' in text:  # we have a measurement with central value and uncertainties
            observable = text.split('=')[0].strip()
            
            # check for prefix
            if observable[0] == '+':
                result['additional'] = True
                observable = observable[1:]

            # parse the formula of parameters
            result['observable'], formula_parameters = parse_formula(observable, parameters, used_parameters)
            if set(formula_parameters.values()) != {None}:
                raise Exception('Unexpected revision of parameter')
            result['parameters'] = set(formula_parameters.keys())

            # check for unit
            data = text.split('=')[1].strip()
            unit = 1
            match = re.search('(e|E)-?[0-9]+$', data)
            if match is not None:
                unit = eval('1' + match.group())
                data = data[:-len(match.group())]

            # parse central value and errors
            result['value'], data, result['precision'] = parse_number(data, unit)
            result['stat_error'], data, precision = parse_number(data, unit)
            result['syst_error'], data, precision = parse_number(data, unit)
            while True:
                error, data, precision = parse_number(data, unit)
                if data is None:
                    break
                match = re.search('\([\w\+-]*\)', data)
                if match is None:
                    result['syst_error'].add(error)
                    result['syst_sum'] = True
                    continue
                if not 'correlations' in result.keys():
                    result['correlations'] = {}
                name = match.group()[1:-1]
                if name not in parameters.keys():
                    if warn_location is not None:
                        print(f'WARNING: nuisance parameter {name} used in {warn_location} is not defined. Adding uncorrelated uncertainty to systematic error.')
                    result['syst_error'].add(error)
                    result['syst_sum'] = True
                    data = data[len(match.group()):].strip()
                    continue
                parameter = parameters[name]
                if parameter.index < 0:
                    parameter.index = len(used_parameters)
                    used_parameters.append(name)
                result['correlations'][name] = error.pos
                data = data[len(match.group()):].strip()

        elif '<' in text:  # we have a limit
            result['observable'], formula_parameters = parse_formula(text.split('<')[0].strip(), parameters, used_parameters)
            if set(formula_parameters.values()) != {None}:
                raise Exception('Unexpected revision of parameter')
            result['parameters'] = set(formula_parameters.keys())

            # check for unit
            data = text.split('<')[1].strip()
            unit = 1
            match = re.search('(e|E)-?[0-9]+', data)
            if match is not None:
                unit = eval('1' + match.group())

            # parse limit value and confidence level
            result['limit'], data, result['precision'] = parse_number(data, unit)
            result['cl'] = eval(re.findall('@\s*([0-9]+)\s*%', data)[0])
            
        return result

    except Exception as exception:
        print(f'ERROR: failed to parse "{text}": {exception.__repr__()}')
        return None
