FROM registry.access.redhat.com/ubi8/python-38

ARG CI_PROJECT_URL
RUN wget -O - https://gitlab.com/api/v4/projects/4207231/packages/generic/graphviz-releases/2.49.3/graphviz-2.49.3.tar.gz | tar xz && cd graphviz-2.49.3 && ./configure --prefix=/opt/app-root && make && make install && cd .. && rm -rf graphviz-2.49.3
RUN python3 -m pip install wheel
RUN python3 -m pip install git+${CI_PROJECT_URL}
