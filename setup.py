from setuptools import setup

__project__ = "averaging"
__version__ = "1.2.0"
__description__ = "a Python module for automated production of HFLAV averages"
__packages__ = ["averaging"]
__author__ = "Heavy Flavor Averaging Group (HFLAV)"
__author_email__ = "hflav-all@cern.ch"
__install_requires__ = ["numpy", "matplotlib", "iminuit", "scipy", "pdg", "requests", "django~=3.2.12", "gunicorn", "setuptools"]
__scripts__ = ["hflav_publication", "hflav_rename", "hflav_webserver", "hflav_overview"]
__package_data__ = {"averaging": ["legend.png", "static/averaging/*", "static/averaging/*/*", "templates/averaging/*.html", "migrations/*"]}

setup(
    name = __project__,
    version = __version__,
    description = __description__,
    packages = __packages__,
    author = __author__,
    author_email = __author_email__,
    install_requires = __install_requires__,
    scripts = __scripts__,
    package_data = __package_data__,
)
