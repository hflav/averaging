# HFLAV Averaging Website Manual

## Finding averages

The main pages shows the list of observables for which averages are provided.
The list can be filtered by selecting an initial state particle and a type of observable (yellow boxes) and any types of child particles (blue boxes).
The minimal number of child particles of each selected type can be set in the red box.
The Clear Filters button removes all filters.

<img src="selection.png">

## Plotting

A overview plot of averages of selected observables can be created by checking the box next to the observable names and clicking "Generate Overview Plot" at the bottom.

## Details

By clicking the observable name in the overview list one can get detailed information about how the average was obtained.
The average is compared with the PDG value if available and all contributing measurements are listed.
Furthermore, all parameters fit together with the observable and their correlation to it are given.